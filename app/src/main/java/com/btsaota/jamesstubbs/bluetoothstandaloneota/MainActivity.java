package com.btsaota.jamesstubbs.bluetoothstandaloneota;

import android.content.res.AssetManager;
import android.os.Bundle;


import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Set;

import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import timber.log.Timber;

public class MainActivity extends Activity {

    private static final int REQUEST_ENABLE_BT = 1;
    private Button onBtn;
    private Button offBtn;
    private Button listBtn;
    private Button findBtn;
    private Button deviceSocketButton;
    private Button getFirmwareVersion;
    private Button updateFirmwareButton;
    private Button toggleAutoReconnect;
    private Button getBatteryStatus;
    private Button getCustomDiag;
    private TextView text;
    private TextView batteryStatus;
    private BluetoothAdapter myBluetoothAdapter;
    private Set<BluetoothDevice> pairedDevices;
    private ArrayList<BluetoothConnection> pairedDevicesConnections;
    private ListView myListView;
    private ArrayAdapter<String> BTArrayAdapter;
    //private String binSourceFile = "FirmwareUpdate-1.1.0.bin";
    //private String binSourceFile = "FirmwareUpdate-1.3.1.bin";
    private String binSourceFile = "Firmware1.1.0ForTesting.bin";
    private InputStream binaryInputStream;
    AssetManager assetManager;
    BluetoothConnection btCon = null;
    Boolean socketConnected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        assetManager = getAssets();
        pairedDevicesConnections = new ArrayList<>();

        // take an instance of BluetoothAdapter - Bluetooth radio
        myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(myBluetoothAdapter == null) {
            onBtn.setEnabled(false);
            offBtn.setEnabled(false);
            listBtn.setEnabled(false);
            findBtn.setEnabled(false);
            text.setText("Status: not supported");

            Toast.makeText(getApplicationContext(),"Your device does not support Bluetooth",
                    Toast.LENGTH_LONG).show();
        } else {
            text = (TextView) findViewById(R.id.text);
            onBtn = (Button)findViewById(R.id.turnOn);
            onBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    on(v);
                }
            });

            offBtn = (Button)findViewById(R.id.turnOff);
            offBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    off(v);
                }
            });

            listBtn = (Button)findViewById(R.id.paired);
            listBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    list(v);
                }
            });

            /*findBtn = (Button)findViewById(R.id.search);
            findBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    find(v);
                }
            });*/

            myListView = (ListView)findViewById(R.id.listView1);

            // create the arrayAdapter that contains the BTDevices, and set it to the ListView
            BTArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
            myListView.setAdapter(BTArrayAdapter);

            batteryStatus = (TextView) findViewById(R.id.batteryStatus);
            batteryStatus.setText("Battery Status: Device socket not yet initialized");

        }
        deviceSocketButton = (Button)findViewById(R.id.deviceSocketButton);
        deviceSocketButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                for (BluetoothDevice device : pairedDevices) {
                    try {
                        //pairedDevicesConnections.add(new BluetoothConnection(device ,myBluetoothAdapter));
                        btCon = new BluetoothConnection(device, myBluetoothAdapter);
                        socketConnected = true;
                        batteryStatus.setText("Press Get Battery Status to display battery status");
                        Timber.d("Connecting socket to " + device.getName() + " " + device.getAddress());
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        getFirmwareVersion = (Button)findViewById(R.id.getFirmwareVersion);
        getFirmwareVersion.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    //pairedDevicesConnections.get(0).getFirmwareVersion();
                    if(socketConnected){
                        Toast.makeText(getApplicationContext(),btCon.getFirmwareVersion() ,
                                Toast.LENGTH_LONG).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Socket not initialized",
                                Toast.LENGTH_LONG).show();
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BadCrcException e) {
                    e.printStackTrace();
                }

            }
        });
        getBatteryStatus = (Button)findViewById(R.id.getBatteryStatus);
        getBatteryStatus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    //pairedDevicesConnections.get(0).getFirmwareVersion();
                    if(socketConnected){
                        batteryStatus.setText("Battery Status: " + btCon.getBatteryStatus());
                        /*Toast.makeText(getApplicationContext(), btCon.getBatteryStatus(),
                            Toast.LENGTH_LONG).show();*/
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Socket not initialized",
                                Toast.LENGTH_LONG).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BadCrcException e) {
                    e.printStackTrace();
                }

            }
        });

        getCustomDiag = (Button)findViewById(R.id.getCustomDiag);
        getCustomDiag.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    //pairedDevicesConnections.get(0).getFirmwareVersion();
                    if(socketConnected){
                        String customDiagnostic = btCon.getCustomDiag();
                        batteryStatus.setText(customDiagnostic);


                        /*Toast.makeText(getApplicationContext(), btCon.getBatteryStatus(),
                            Toast.LENGTH_LONG).show();*/
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Socket not initialized",
                                Toast.LENGTH_LONG).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BadCrcException e) {
                    e.printStackTrace();
                }

            }
        });

      updateFirmwareButton = (Button)findViewById(R.id.updateFirmware);
        updateFirmwareButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(socketConnected){
                    updateFirmware(btCon);
                }
                else{
                    Toast.makeText(getApplicationContext(),"Socket not initialized",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        toggleAutoReconnect = (Button)findViewById(R.id.autoReconnectToggle);
        toggleAutoReconnect.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(socketConnected){
                    try {
                        int receive = btCon.toggleAutoReconnect(btCon.getAutoReconnectStatus());
                        Timber.d("Auto Reconnect Command Response: %d", receive);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (BadCrcException e) {
                        e.printStackTrace();
                    }

                }
                else{
                    Toast.makeText(getApplicationContext(),"Socket not initialized",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void on(View view){
        if (!myBluetoothAdapter.isEnabled()) {
            Intent turnOnIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOnIntent, REQUEST_ENABLE_BT);

            Toast.makeText(getApplicationContext(),"Bluetooth turned on" ,
                    Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(getApplicationContext(),"Bluetooth is already on",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if(requestCode == REQUEST_ENABLE_BT){
            if(myBluetoothAdapter.isEnabled()) {
                text.setText("Status: Enabled");
            } else {
                text.setText("Status: Disabled");
            }
        }
    }

    public void list(View view){
        // get paired devices
        pairedDevices = myBluetoothAdapter.getBondedDevices();

        // put it's one to the adapter
        for(BluetoothDevice device : pairedDevices){
            BTArrayAdapter.add(device.getName()+ "\n" + device.getAddress());
            //pairedDevicesConnections.add(new BluetoothConnection(device ,myBluetoothAdapter));
        }


        Toast.makeText(getApplicationContext(),"Show Paired Devices",
                Toast.LENGTH_SHORT).show();

    }

    final BroadcastReceiver bReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // add the name and the MAC address of the object to the arrayAdapter
                BTArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                BTArrayAdapter.notifyDataSetChanged();
            }
        }
    };

    public void find(View view) {
        if (myBluetoothAdapter.isDiscovering()) {
            // the button is pressed when it discovers, so cancel the discovery
            myBluetoothAdapter.cancelDiscovery();
        }
        else {
            BTArrayAdapter.clear();
            myBluetoothAdapter.startDiscovery();

            registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        }
    }

    public void off(View view){
        myBluetoothAdapter.disable();
        text.setText("Status: Disconnected");

        Toast.makeText(getApplicationContext(),"Bluetooth turned off",
                Toast.LENGTH_LONG).show();
    }

    private void updateFirmware(BluetoothConnection btConnection){
        String out;
        int packetNumber = 0;
        int response;
        int pageLength;
        try {
            binaryInputStream = assetManager.open(binSourceFile);
            response = btConnection.sendFirstPacket(binaryInputStream);
            if(response == 0){
                out = String.format("Response commandID: %d",response);
                Toast.makeText(getApplicationContext(),out,
                        Toast.LENGTH_LONG).show();
                Timber.d("" + out);
                pageLength = btConnection.getPageLength();

                while(response == 0 && packetNumber<pageLength){
                   response = btConnection.sendFirmwareBusinessPacket(packetNumber);
                    out = String.format("Response commandID: %d for packet number: %d",response, packetNumber);
                    /*Toast.makeText(getApplicationContext(),out,
                            Toast.LENGTH_LONG).show();*/
                    Timber.d("" + out);
                    packetNumber++;
                }
            }
            else{
                Toast.makeText(getApplicationContext(),"OTA failed",
                        Toast.LENGTH_LONG).show();
                Timber.d("OTA Failed");
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (BadCrcException e) {
            e.printStackTrace();
        } /*catch (TimeoutException e) {
            Timber.d(e.getMessage());
            Toast.makeText(getApplicationContext(),e.getMessage(),
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }*/
    }


    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        unregisterReceiver(bReceiver);
    }


}
