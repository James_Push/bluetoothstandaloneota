package com.btsaota.jamesstubbs.bluetoothstandaloneota.btWrappers;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import timber.log.Timber;

/**
 * Source from http://stackoverflow.com/questions/18657427/
 */
public class BluetoothConnector {
  private BluetoothSocketWrapper bluetoothSocket;
  private BluetoothDevice device;
  private boolean secure;
  private BluetoothAdapter adapter;
  private List<UUID> uuidCandidates;
  private int candidate;

  /**
   * @param device
   *     the device
   * @param secure
   *     if connection should be done via a secure socket
   * @param adapter
   *     the Android BT adapter
   * @param uuidCandidates
   *     a list of UUIDs. if null or empty, the Serial PP id is used
   */
  public BluetoothConnector(BluetoothDevice device, boolean secure, BluetoothAdapter adapter,
                            List<UUID> uuidCandidates) {
    this.device = device;
    this.secure = secure;
    this.adapter = adapter;
    this.uuidCandidates = uuidCandidates;

    if (this.uuidCandidates == null || this.uuidCandidates.isEmpty()) {
      this.uuidCandidates =
          Collections.singletonList(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
    }
  }

  public BluetoothSocketWrapper connect() throws IOException {
    boolean success = false;
    while (selectSocket()) {
      adapter.cancelDiscovery();

      try {
        bluetoothSocket.connect();
        success = true;
        break;
      } catch (IOException e) {
        //try the fallback
        FallbackBluetoothSocket fallbackBluetoothSocket = null;
        try {
          //bluetoothSocket.close();
          bluetoothSocket = new FallbackBluetoothSocket(bluetoothSocket.getUnderlyingSocket());
          Thread.sleep(500);
          bluetoothSocket.connect();
          success = true;
          break;
        } catch (FallbackException e1) {
          Timber.i(e, "Could not initialize FallbackBluetoothSocket classes.");
        } catch (InterruptedException e1) {
          Timber.i(e1, "Interrupted while waiting for Fallback Bluetooth connection");
        } catch (IOException e1) {
          Timber.i(e1, "Fallback failed. Cancelling.");
        }
      }
    }

    if (!success) {
      throw new IOException("Could not connect to device: " + device.getAddress());
    }

    return bluetoothSocket;
  }

  private boolean selectSocket() throws IOException {
    if (candidate >= uuidCandidates.size()) {
      return false;
    }

    BluetoothSocket tmp;
    UUID uuid = uuidCandidates.get(candidate++);

    Timber.i("Attempting to connect to Protocol: %s", uuid);
    if (secure) {
      tmp = device.createRfcommSocketToServiceRecord(uuid);
    } else {
      tmp = device.createInsecureRfcommSocketToServiceRecord(uuid);
    }
    bluetoothSocket = new NativeBluetoothSocket(tmp);

    return true;
  }
}
