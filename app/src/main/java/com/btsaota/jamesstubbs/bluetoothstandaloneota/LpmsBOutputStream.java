package com.btsaota.jamesstubbs.bluetoothstandaloneota;

import com.google.common.io.ByteStreams;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import okio.Buffer;
import timber.log.Timber;

/**
 * @author Sebastian Mocny
 */
public class LpmsBOutputStream extends FilterOutputStream {
  private static final int PAGE_SIZE = 256;
  byte[] fileData;
  int fileSize;
  int pages;
  int remainder;


  public LpmsBOutputStream(OutputStream out) {
    super(out);
  }

  public void write(LpmsBData lpmsBData) throws IOException {
    lpmsBData.writeToBuffer(new Buffer()).writeTo(this);
  }

  public void writeFirmware(InputStream is) throws IOException {
    int fileSize = is.available();
    byte[] fileData = new byte[fileSize];
    int pages = (fileSize + PAGE_SIZE - 1) / PAGE_SIZE;

    ByteStreams.readFully(is, fileData);

    Timber.i("File Length: %s", fileSize);
    Timber.i("Number of pages: %s", pages);

    Buffer b = new Buffer();

    for (int i = 0; i < 1; ++i) {
      new LpmsBData(
          2,
              Arrays.copyOfRange(fileData, i * PAGE_SIZE, (1 + i) * PAGE_SIZE)
      ).writeToBuffer(b);
    }

    b.writeTo(this);
    b.flush();
  }

  public boolean sendFirstPacket(InputStream fileInputStream) throws IOException {

    fileSize = fileInputStream.available();
    fileData = new byte[fileSize];

    pages = fileSize  / PAGE_SIZE;
    //pages = 237;

    remainder = fileSize % PAGE_SIZE;

    if (remainder > 0) ++pages;
    if(pages > 512){
      Timber.d("Too many pages.  There must be no more than 512 pages in the firmware update.  Check bin file.");
      return false;
    }

    ByteStreams.readFully(fileInputStream, fileData);

    Timber.d("File Length: %s", fileSize);
    Timber.d("Number of pages: %s", pages);

    write(new LpmsBData(2, ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(pages).array()));
    fileInputStream.close();
    return true;
  }

  public void sendFirmwareBusinessPacket(int i) throws IOException {

    byte[] pageBytes = Arrays.copyOfRange(fileData, i * PAGE_SIZE, (1 + i) * PAGE_SIZE);
    /*for (byte j: pageBytes
            ) {
      Timber.d("" + j);
    }*/

    write(new LpmsBData(
            2, pageBytes
    ));
    Timber.e("Page %d sent to device", i);
  }
}
