package com.btsaota.jamesstubbs.bluetoothstandaloneota.btWrappers;

import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Source from http://stackoverflow.com/questions/18657427/
 */
public interface BluetoothSocketWrapper {
  InputStream getInputStream() throws IOException;

  OutputStream getOutputStream() throws IOException;

  String getRemoteDeviceName();

  void connect() throws IOException;

  String getRemoteDeviceAddress();

  void close() throws IOException;

  BluetoothSocket getUnderlyingSocket();
}
