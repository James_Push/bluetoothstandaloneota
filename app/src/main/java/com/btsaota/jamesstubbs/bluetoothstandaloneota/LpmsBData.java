package com.btsaota.jamesstubbs.bluetoothstandaloneota;

import com.google.common.base.Objects;
import com.google.common.io.BaseEncoding;

import java.util.Arrays;

import okio.Buffer;
import timber.log.Timber;

/**
 * A single packet of data read from the LPMS-B data stream.
 *
 * @author kwogger
 */
public class LpmsBData {
  public int deviceId;
  public int commandId;
  public byte[] data;

  public LpmsBData() {
    // do nothing, used for decoding.
  }

  public LpmsBData(int commandId) {
    deviceId = 1;
    this.data = new byte[0];
    this.commandId = commandId;
  }

  public LpmsBData(int commandId, byte[] data) {
    this.data = data;
    this.commandId = commandId;
    deviceId = 1;
  }

  public char generateCrc() {
    char sum = 0;

    sum += sumOfBytes(deviceId);
    sum += sumOfBytes(commandId);
    if (data != null) {
      sum += sumOfBytes(data.length);
      for (byte b : data) {
        sum += b & 0xFF;
        //Timber.d(" "+ (int)sum);
      }
    }
    //Timber.d("The CRC check yields: " + (int)sum);
    return sum;
  }

  public static int sumOfBytes(int s) {
    //return (s & 0xFF) + ((s >> 8) & 0xFF) + ((s >> 16) & 0xFF) + ((s >> 24) & 0xFF);
    return s;
  }

  public Buffer writeToBuffer(Buffer b) {
    b.writeByte(0x3a);
    b.writeByte(deviceId);
    b.writeByte(commandId);
    if (data == null) {
      b.writeShortLe(0);
    } else {
      b.writeShortLe(data.length);
      b.write(data);
    }
    b.writeShortLe(generateCrc());
    b.writeByte(0x0D);
    b.writeByte(0x0A);

    return b;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this)
        .add("deviceId", deviceId)
        .add("commandId", commandId)
        .add("data", data == null ? null : BaseEncoding.base16().encode(data))
        .add("crc", Integer.toHexString(generateCrc()))
        .toString();
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(deviceId, commandId, data);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final LpmsBData other = (LpmsBData) obj;
    return Objects.equal(this.deviceId, other.deviceId)
        && Objects.equal(this.commandId, other.commandId)
        && Arrays.equals(this.data, other.data);
  }
}
