package com.btsaota.jamesstubbs.bluetoothstandaloneota;

import android.os.SystemClock;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * InputStream wrapper that allows you to read data packets from a LPMS-B byte stream.
 *
 * @author kwogger
 */
public class LpmsBInputStream extends FilterInputStream {
  InputStream in;

  public LpmsBInputStream(InputStream in) {
    super(in);
    this.in = in;
  }

  @Override
  public int read() throws IOException {
    int i = super.read();


    if (i == -1) {

      throw new EOFException();
    }
    return i;
  }

  @Override
  public int read(byte[] buffer) throws IOException {
    int i = super.read(buffer);
    if (i == -1) {
      throw new EOFException();
    }
    return i;
  }

  @Override
  public int read(byte[] buffer, int byteOffset, int byteCount) throws IOException {
    int i = super.read(buffer, byteOffset, byteCount);
    if (i == -1) {
      throw new EOFException();
    }
    return i;
  }

  public LpmsBData readData() throws IOException, BadCrcException {
    // TODO: handle end-of-stream via read() returning -1
    int b;
   /* long timer = SystemClock.uptimeMillis();*/
    do {
      b = read();
      /*if(SystemClock.uptimeMillis() > (timer + 5000)){
        return null;
      }*/
    } while (b != 0x3A);

    LpmsBData packet = new LpmsBData();
    packet.deviceId = read();
    packet.commandId = read();
    int dataLength = read();
    dataLength |= (read() << 8);
    packet.data = new byte[dataLength];

    int bytesRead;
    int remaining = dataLength;
    while ((bytesRead = read(packet.data, dataLength - remaining, remaining)) != -1
        && remaining > 0) {
      remaining -= bytesRead;
    }

    char crc = (char) read();
    crc |= (read() << 8);

    b = read();
    if (b != 0x0D) {
      throw new FramingErrorException("bad first end byte" + b);
    }
    b = read();
    if (b != 0x0A) {
      throw new FramingErrorException("bad second end byte: " + b);
    }

    if (crc != packet.generateCrc()) {
      throw new BadCrcException(
          "bad crc, given: " + Integer.toHexString(crc)
              + " expected: " + Integer.toHexString(packet.generateCrc()));
    }

    return packet;
  }
  public InputStream getUnderlyingInputStream(){
    return in;
  }
}
