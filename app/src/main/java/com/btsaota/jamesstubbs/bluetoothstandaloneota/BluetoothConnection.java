package com.btsaota.jamesstubbs.bluetoothstandaloneota;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.SystemClock;

import com.btsaota.jamesstubbs.bluetoothstandaloneota.btWrappers.BluetoothConnector;
import com.btsaota.jamesstubbs.bluetoothstandaloneota.btWrappers.BluetoothSocketWrapper;
import com.google.common.io.ByteStreams;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.TimeoutException;


import timber.log.Timber;

/**
 * Created by James Stubbs on 05/11/2015.
 */
public class BluetoothConnection extends Thread {
    private final BluetoothSocketWrapper mmSocket;
    private LpmsBOutputStream os;
    private LpmsBInputStream is;
    private BufferedInputStream bIS;
    private boolean autoReconnectStatus = false;


    public BluetoothConnection(BluetoothDevice device, BluetoothAdapter adapter) throws IOException {


        BluetoothConnector btConnector = new BluetoothConnector(device, true, adapter, null);
        mmSocket = btConnector.connect();


        if(mmSocket.getUnderlyingSocket().isConnected()){
            os =
                    new LpmsBOutputStream(
                            new BufferedOutputStream(mmSocket.getOutputStream()));

        }
        else{
            Timber.i("Socket is not connected");
        }

        if(mmSocket.getUnderlyingSocket().isConnected()){

            is =
                    new LpmsBInputStream(
                            new BufferedInputStream(mmSocket.getInputStream()));

        }
        else{
            Timber.i("Socket is not connected");
        }

    }

    public String getFirmwareVersion() throws IOException, BadCrcException {
        String outputString;
        if(os != null){
            os.write(new LpmsBData(66));
            os.flush();
        }
        else{
            return "No output stream";
        }

        byte[] rxBuffer = is.readData().data;

        int pt1 = rxBuffer[3];
        int pt2 = rxBuffer[4];
        int pt3 = rxBuffer[5];

        /*int o = 0;
        int pt1 = rxBuffer[o+3] << 24 | rxBuffer[o+2] << 16 | rxBuffer[o+1] << 8 | rxBuffer[o]; o += 4;
        int pt2 = rxBuffer[o+3] << 24 | rxBuffer[o+2] << 16 | rxBuffer[o+1] << 8 | rxBuffer[o]; o += 4;
        int pt3 = rxBuffer[o+3] << 24 | rxBuffer[o+2] << 16 | rxBuffer[o+1] << 8 | rxBuffer[o];*/

        outputString = String.format("Firmware version: %d %d %d", pt1, pt2, pt3);
        Timber.d(outputString);
        return outputString;


    }

    String getBatteryStatus() throws IOException, BadCrcException {
        String outputString;
        //is.skip(is.available());
        Timber.d("" + is.available());


        if(os != null){
            os.write(new LpmsBData(60));
            os.flush();
        }
        else{
            return "No output stream";
        }

        LpmsBData receivedPacket = is.readData();
        // read through and skip over any garbage data in the inputstream
        while(receivedPacket.commandId != 60){
           receivedPacket = is.readData();
        }
        byte[] rxBuffer = receivedPacket.data;

        /*for (byte j: rxBuffer
            ) {
      Timber.d("" + j);
    }*/
        for(int i = 0; i < rxBuffer.length; i++){
            int pt = (rxBuffer[i+1] & 0xFF) << 8 | (rxBuffer[i] & 0xFF);
            Timber.d("Battery diagnostic %d: %d", i, pt);
            i++;
        }
        int pt1 = (rxBuffer[29] & 0xFF) << 8 | (rxBuffer[28] & 0xFF);
        int pt2 = (rxBuffer[9] & 0xFF) << 8 | (rxBuffer[8] & 0xFF);

        outputString = String.format("Battery Percentage: %d %% \n Battery Voltage: %d mV", pt1, pt2);
        return outputString;
    }

    String getCustomDiag() throws IOException, BadCrcException {
        String outputString;

        if(os != null){
            os.write(new LpmsBData(46));
            os.flush();
        }
        else{
            return "No output stream";
        }

        LpmsBData receivedPacket = is.readData();
        // read through and skip over any garbage data in the inputstream
        while(receivedPacket.commandId != 46){
            receivedPacket = is.readData();
        }
        byte[] rxBuffer = receivedPacket.data;

       /* for (byte j: rxBuffer
            ) {
      Timber.d("" + j);
    }*/
        char[] diag = new char[8];
        String diagString = "";
        for(int i = 0; i<8; i++){
            diag[i] = (char)(rxBuffer[i+172]);
            if(diag[i] != 0){
                diagString = diagString + diag[i];
            }
        }
        if(diagString.equals("false")){
            autoReconnectStatus = false;
        }
        if(diagString.equals("true")){
            autoReconnectStatus = true;
        }

        outputString = String.format("isAutoReconnectEnabled: %s ", diagString);
        Timber.d(outputString);
        return outputString;
    }

    public int sendFirstPacket(InputStream fileInputStream) throws IOException, BadCrcException /*, TimeoutException*/ {
        boolean receive = false;
        if(os != null){
            receive = os.sendFirstPacket(fileInputStream);
            os.flush();
        }
        if(receive){
            return is.readData().commandId;
        }
        else{
            return -1;
        }


    }
    public int sendFirmwareBusinessPacket(int i) throws IOException, BadCrcException {
        if(os != null){
            os.sendFirmwareBusinessPacket(i);
            os.flush();
        }

        return is.readData().commandId;
    }
    public int getPageLength(){
        return os.pages;
    }

    public int toggleAutoReconnect(boolean toggle) throws IOException, BadCrcException {
        if(os != null){
            if(toggle){
                os.write(new LpmsBData(24));
                Timber.d("Sending Auto Reconnect disable command");
            }
            else{
                os.write(new LpmsBData(23));
                Timber.d("Sending Auto Reconnect enable command");
            }
            os.flush();
        }

        return is.readData().commandId;
    }

    public boolean getAutoReconnectStatus(){
        return autoReconnectStatus;
    }
}
