package com.btsaota.jamesstubbs.bluetoothstandaloneota.btWrappers;

/**
 * Source from http://stackoverflow.com/questions/18657427/
 */
public class FallbackException extends Exception {
  private static final long serialVersionUID = 1L;

  public FallbackException(Exception e) {
    super(e);
  }
}
