package com.btsaota.jamesstubbs.bluetoothstandaloneota;

import java.io.IOException;

/**
 * Thrown when a framing error is encountered while attempting to read a packet.
 *
 * @author kwogger
 */
public class FramingErrorException extends IOException {
  public FramingErrorException(String s) {
    super(s);
  }
}
