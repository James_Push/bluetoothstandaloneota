package com.btsaota.jamesstubbs.bluetoothstandaloneota;

/**
 * Thrown when a packet fails its CRC check.
 *
 * @author kwogger
 */
public class BadCrcException extends Exception {
  public BadCrcException(String s) {
    super(s);
  }
}
